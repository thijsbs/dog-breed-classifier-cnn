import os
import torch
import torchvision.models as tvmodels


import torch.nn as nn
import torch.nn.functional as F


class DogNet(nn.Module):
    """The dog classifier model definition from scratch

    I wanted to keep the model relatively simple with 3 convolutional layers
    and 3 fully connected layers.
    """

    def __init__(self):
        super(DogNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, 5, padding=2) 
        self.conv2 = nn.Conv2d(64, 128, 3, padding=1) 
        self.conv3 = nn.Conv2d(128, 256, 3, padding=0) 

        self.fc1 = nn.Linear(3*3 * 256, 800)   
        self.fc2 = nn.Linear(800, 300)
        self.fc3 = nn.Linear(300, 133)
        
        self.pool = nn.MaxPool2d(4, 4)
        self.dropout = nn.Dropout(0.25)

    
    def forward(self, x):
        """Feedforward behavior"""

        x = self.pool(F.relu(self.conv1(x))) # 56
        x = self.pool(F.relu(self.conv2(x))) # 14
        x = self.pool(F.relu(self.conv3(x))) # 3

        x = x.view(-1, 3*3*256)
        x = self.dropout(x)
        x = F.relu(self.fc1(x))
        x = self.dropout(x)
        x = F.relu(self.fc2(x))
        x = self.dropout(x)
        x = self.fc3(x)
        return x


def normal_vgg16_network():
    return tvmodels.vgg16(pretrained=True)


def model_scratch_untrained():
    return DogNet()


def load_model_scratch(fp):
    """Load a pre-trained model_scratch dog classifier"""
    model_scratch = model_scratch_untrained()
    if not os.path.isfile(fp):
        msg = "The path {!r} does not contain a state dict for model_scratch"
        print("---------")
        print(msg.format(fp))
        print("---------")
        return
    model_scratch.load_state_dict(torch.load(fp, map_location=torch.device('cpu')))
    return model_scratch


def model_transfer_untrained(pretrained_features=False):
    """Get the definition of the model_transfer. 

    When loading the full classifier mode it is not necessary to fetch 
    the vgg16 weights.
    For normal use of the network keep pretrained_features False, for 
    traning the transfer learnring network use pretrained_features True.
    """
    model_transfer = tvmodels.vgg16(pretrained_features)

    for param in model_transfer.features.parameters():
        param.require_grad = False
    model_transfer.classifier[6] = nn.Linear(model_transfer.classifier[6].in_features, 133)
    return model_transfer


def load_model_transfer(fp):
    """Load a pre-trained model_transfer dog classifier"""
    model_transfer = model_transfer_untrained(pretrained_features=False)
    if not os.path.isfile(fp):
        msg = "The path {!r} does not contain a state dict for model_transfer"
        print("---------")
        print(msg.format(fp))
        print("---------")
        return
    model_transfer.load_state_dict(torch.load(fp, map_location=torch.device('cpu')))
    return model_transfer

