import os
import torch
import numpy as np
import torch.optim as optim
import torch.nn as nn
import torchvision.transforms as transforms
from matplotlib import pyplot as plt
from torchvision import datasets

import models

def train(n_epochs, loaders, model, optimizer, criterion, use_cuda, save_path=None):
    """returns trained model"""
    # initialize tracker for minimum validation loss
    valid_loss_min = np.Inf 
    for epoch in range(1, n_epochs+1):
        # initialize variables to monitor training and validation loss
        train_loss = 0.0
        valid_loss = 0.0
        ###################
        # train the model #
        ###################
        model.train()
        for batch_idx, (data, target) in enumerate(loaders['train']):
            # move to GPU
            if use_cuda:
                data, target = data.cuda(), target.cuda()
            ## find the loss and update the model parameters accordingly
            ## record the average training loss, using something like
            optimizer.zero_grad()
            output = model(data)
            loss = criterion(output, target)
            loss.backward()
            optimizer.step()
            train_loss = train_loss + ((1 / (batch_idx + 1)) * (loss.data - train_loss))
            
        ######################    
        # validate the model #
        ######################
        model.eval()
        for batch_idx, (data, target) in enumerate(loaders['valid']):
            # move to GPU
            if use_cuda:
                data, target = data.cuda(), target.cuda()
            output = model(data)
            vloss = criterion(output, target)
            valid_loss = valid_loss + ((1 / (batch_idx + 1)) * (loss.data - valid_loss))
            ## update the average validation loss

        # print training/validation statistics 
        print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f}'.format(
            epoch, 
            train_loss,
            valid_loss
            ))
        
        # save the model if validation loss has decreased
        if valid_loss <= valid_loss_min and save_path is not None:
            print("Validation loss decreased... Saving model")
            torch.save(model.state_dict(), save_path)
            valid_loss_min = valid_loss
        
    return model



def dataloaders(dr, batch_size=20):

    ## Specify appropriate transforms, and batch_sizes
    train_transform = transforms.Compose([transforms.RandomHorizontalFlip(),
                                         transforms.RandomRotation(15),
                                         transforms.Resize(212),
                                         transforms.CenterCrop(224),
                                         transforms.ToTensor(),
                                         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    valid_transform = transforms.Compose([transforms.Resize(212),
                                          transforms.CenterCrop(224),
                                          transforms.ToTensor(),
                                          transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    dog_train = datasets.ImageFolder(os.path.join(dr, "train"), transform=train_transform)
    dog_valid = datasets.ImageFolder(os.path.join(dr, "valid"), transform=valid_transform)
    dog_test = datasets.ImageFolder(os.path.join(dr, "test"), transform=valid_transform)

    data_loaders = {"train": torch.utils.data.DataLoader(dog_train, batch_size=batch_size, shuffle=True),
                    "valid": torch.utils.data.DataLoader(dog_valid, batch_size=batch_size, shuffle=True),
                    "test" : torch.utils.data.DataLoader(dog_test, batch_size=batch_size, shuffle=True)}

    return data_loaders



def criterion():
    # For now I have only defined the cross entropy loss
    return nn.CrossEntropyLoss()


def optimizer(model, learning_rate=0.01):
    if isinstance(model, models.DogNet):
        optimizer = optim.SGD(model.parameters(), lr=learning_rate)
    else:
        optimizer = optim.SGD(model.classifier.parameters(), lr=learning_rate)
    return optimizer
